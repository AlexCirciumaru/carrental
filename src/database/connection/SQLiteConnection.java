package database.connection;

import java.sql.*;
import javax.swing.*;

public class SQLiteConnection {
	
	Connection conn = null;
	
	public static Connection dbConnector() {
		
		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:D:\\JavaPrograms\\CarRental\\Accounts.sqlite");
			//JOptionPane.showMessageDialog(null, "Connection Successfull.");
			return conn;
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, e);
			return null;
		}
	}
}
