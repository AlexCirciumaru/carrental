package database.connection;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class CustomersDataBase {
	Connection conn = null;
	
	/**
	 * Conexiunea la baza de date ce contine inchirierile active.
	 * @return returneaza null in caz ca nu se poate conecta la baza de date,conexiunea in cazul in care reuseste
	 */
	public static Connection dbConnector() {

		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:D:\\JavaPrograms\\CarRental\\Customers.sqlite");
			// JOptionPane.showMessageDialog(null, "Connection Successfull.");
			return conn;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			return null;
		}
	}
}
