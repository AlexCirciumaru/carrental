package database.connection;

import java.sql.*;
import javax.swing.*;

public class CarsDBConnection {
	
	Connection conn = null;

	public static Connection CarsdbConnector() {

		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:D:\\JavaPrograms\\CarRental\\SQLite");
			//JOptionPane.showMessageDialog(null, "Connection Successfull.");
			return conn;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			return null;
		}
	}
}
