package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTable;

import database.connection.CarsDBConnection;


public class FillCarsJTable {
	public static Connection connection = CarsDBConnection.CarsdbConnector();

	public static void fillTheJTable(JTable table) {
		try {
			String query = "select * from cars";
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			table.setModel(SetTableModel.resultSetToTableModel(rs));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
