package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import database.connection.CarsDBConnection;

public class CheckCarID {
	public static Connection connection = CarsDBConnection.CarsdbConnector();
	public static boolean carIDexists = false;

	public static boolean checkCarID(String carID) {

		PreparedStatement st = null;
		ResultSet r1 = null;
		try {
			st = connection.prepareStatement("select * from cars where CarID = ?");
			st.setString(1, carID);
			r1 = st.executeQuery();
			if (r1.next()) {
				carIDexists = true;
				return carIDexists;
			}
			return carIDexists;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				st.close();
				r1.close();
			} catch (Exception e2) {
				// stay silent
			}
		}
	}
}
