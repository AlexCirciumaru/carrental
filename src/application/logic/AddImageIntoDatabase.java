package application.logic;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.connection.CarsDBConnection;

public class AddImageIntoDatabase {
	
	static Connection connection = CarsDBConnection.CarsdbConnector();
	
	public static void addPicture(String filename) {
		
		String query = "insert into cars (Image) values (?)";
		try (Connection conn = CarsDBConnection.CarsdbConnector();
				PreparedStatement pstmt = conn.prepareStatement(query)) {

			// set parameters
			pstmt.setBytes(1, readFile(filename));

			pstmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static byte[] readFile(String file) {
		ByteArrayOutputStream bos = null;
		FileInputStream fis = null;
		try {
			File f = new File(file);
			fis = new FileInputStream(f);
			byte[] buffer = new byte[4096];
			bos = new ByteArrayOutputStream();
			for (int len; (len = fis.read(buffer)) != -1;) {
				bos.write(buffer, 0, len);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e2) {
			System.err.println(e2.getMessage());
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
				// stay silent
			}
		}
		return bos != null ? bos.toByteArray() : null;
	}
}
