package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTable;

import database.connection.CustomersDataBase;
import net.proteanit.sql.DbUtils;

public class ShowDatabaseInJTable {

	public static Connection connection = CustomersDataBase.dbConnector();

	public static void fillTheJTable(JTable table) {
		try {
			String query = "select * from customers";
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
