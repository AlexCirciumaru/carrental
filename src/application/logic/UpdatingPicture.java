package application.logic;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import app.gui.CarsManagement;
import database.connection.CarsDBConnection;

public class UpdatingPicture {
	
	private byte[] readFile(String file) {
		ByteArrayOutputStream bos = null;
		FileInputStream fis = null;
		try {
			File f = new File(file);
			fis = new FileInputStream(f);
			byte[] buffer = new byte[4096];
			bos = new ByteArrayOutputStream();
			for (int len; (len = fis.read(buffer)) != -1;) {
				bos.write(buffer, 0, len);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e2) {
			System.err.println(e2.getMessage());
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
				// stay silent
			}
		}
		return bos != null ? bos.toByteArray() : null;
	}
	
	/**
	 * Functia primeste ca parametru calea catre imaginea dorita.
	 * Se realizeaza conexiunea la baza de date si se modifica imaginea in functie de CarID-ul introdus.
	 * @param filename
	 */
	public void updatePicture(String filename) {
		// update sql
		String updateSQL = "UPDATE cars " + "SET Image = ? " + "WHERE CarID='" + CarsManagement.txtCarID.getText() + "' ";
		PreparedStatement pstmt = null;
		try {

			Connection conn = CarsDBConnection.CarsdbConnector();
			pstmt = conn.prepareStatement(updateSQL);
			// set parameters
			pstmt.setBytes(1, readFile(filename));

			pstmt.executeUpdate();
			System.out.println("Stored the file in the BLOB column.");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				pstmt.close();
			} catch (Exception e2) {
				// stay silent
			}
		}
	}
}
