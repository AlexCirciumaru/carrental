package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JComboBox;

import database.connection.CarsDBConnection;

public class FillSalesComboBox {
	PreparedStatement pst = null;
	ResultSet rs = null;
	Connection connection = null;
	JComboBox<String> ComboBoxCar;

	public FillSalesComboBox() {
		connection = CarsDBConnection.CarsdbConnector();
	}

	public void fillComboBox(JComboBox<String> ComboBoxCar,String txt) {
		try {
			String query = "select * from cars where OnSale='" + txt + "' ";
			pst = connection.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				ComboBoxCar.addItem(rs.getString("Car_Name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				rs.close();
			} catch (Exception e2) {
				// stay silent
			}
		}
	}
}
