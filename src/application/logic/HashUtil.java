package application.logic;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;


public class HashUtil {
	
	/**
	 * Codul care se ocupa de criptarea parolei.
	 * @param originalString este preluat si criptat cu algoritmul sha256.
	 * @return returneaza parola criptata care pe urma este introdusa in baza de date.
	 */
	public static String sha256(String originalString) {
		if (originalString == null || originalString.isEmpty()) {
			return null;
		}
		String hashedString = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
			return bytesToHex(encodedhash);
		} catch (Exception e) {
			// Stay silent.
		}
		return hashedString;
	}

	private static String bytesToHex(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}
}
