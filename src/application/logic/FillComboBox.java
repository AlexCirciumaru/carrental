package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import javax.swing.JComboBox;

import database.connection.CarsDBConnection;

public class FillComboBox {
	Connection connection = null;
	JComboBox<String> ComboBoxCar;
	
	public FillComboBox() {
		connection = CarsDBConnection.CarsdbConnector();
	}
	
	public void fillComboBox(JComboBox<String> ComboBoxCar,String txt) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			String query = "select * from cars where OnSale='" + txt + "' ";
			pst = connection.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				ComboBoxCar.addItem(rs.getString("Car_Name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				pst.close();
				rs.close();
			} catch (Exception e2) {
				// stay silent
			}
		}
	}
}
