package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import database.connection.SQLiteConnection;

public class CheckTheUsername {

	public static Connection connection = SQLiteConnection.dbConnector();

	public static boolean checkUsername(String username) {
		
		boolean usernameExists = false;
		PreparedStatement st = null;
		ResultSet r1 = null;
		try {
			st = connection.prepareStatement("select * from accounts where Username = ?");
			st.setString(1, username);
			r1=st.executeQuery();
			if(r1.next()) {
			  usernameExists = true;
			  return usernameExists;
			}
			return usernameExists;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try {
				st.close();
				r1.close();
			} catch (Exception e2) {
				// stay silent
			}
		}
	}
}
