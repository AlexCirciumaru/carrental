package application.logic;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;

import app.gui.CarReservation;
import database.connection.CarsDBConnection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class ShowCarDetails {

	private JFrame frame;
	private JTextField txtPricePerDay,txtFuel,txtYearFab,txtAirCond,txtColour,txtDoorsNr,txtSeatsNr,txtCarName,txtOnSale;
	Connection connection = null;
	CarReservation carReservation = new CarReservation();
	JComboBox<String> ComboBoxCar;
	JLabel labelShowImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowCarDetails window = new ShowCarDetails();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ShowCarDetails() {
		initialize();
		connection = CarsDBConnection.CarsdbConnector();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 820, 556);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCarDetails = new JLabel("Car Details :");
		lblCarDetails.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));
		lblCarDetails.setBounds(211, 20, 125, 35);
		frame.getContentPane().add(lblCarDetails);
		
		JLabel lblCarName = new JLabel("Car Name :");
		lblCarName.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblCarName.setBounds(10, 80, 104, 29);
		frame.getContentPane().add(lblCarName);
		
		JLabel lblSeatsNr = new JLabel("Seats Nr. :");
		lblSeatsNr.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblSeatsNr.setBounds(10, 130, 91, 20);
		frame.getContentPane().add(lblSeatsNr);
		
		JLabel lblDoorsNr = new JLabel("Doors Nr. :");
		lblDoorsNr.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblDoorsNr.setBounds(10, 180, 104, 20);
		frame.getContentPane().add(lblDoorsNr);
		
		JLabel lblColour = new JLabel("Colour :");
		lblColour.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblColour.setBounds(10, 230, 79, 20);
		frame.getContentPane().add(lblColour);
		
		JLabel lblAirCond = new JLabel("Air Cond. :");
		lblAirCond.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblAirCond.setBounds(10, 280, 104, 20);
		frame.getContentPane().add(lblAirCond);
		
		JLabel lblYearFab = new JLabel("Year Fab. :");
		lblYearFab.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblYearFab.setBounds(10, 330, 104, 20);
		frame.getContentPane().add(lblYearFab);
		
		JLabel lblFuel = new JLabel("Fuel :");
		lblFuel.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblFuel.setBounds(10, 380, 56, 20);
		frame.getContentPane().add(lblFuel);
		
		JLabel lblPricePerDay = new JLabel("Price Per Day(\u20AC) :");
		lblPricePerDay.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblPricePerDay.setBounds(10, 430, 155, 20);
		frame.getContentPane().add(lblPricePerDay);
		
		JLabel lblImage = new JLabel("Image :");
		lblImage.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblImage.setBounds(511, 80, 79, 21);
		frame.getContentPane().add(lblImage);
		
		txtPricePerDay = new JTextField();
		txtPricePerDay.setBounds(168, 430, 69, 25);
		frame.getContentPane().add(txtPricePerDay);
		txtPricePerDay.setColumns(10);
		
		txtFuel = new JTextField();
		txtFuel.setBounds(168, 380, 69, 25);
		frame.getContentPane().add(txtFuel);
		txtFuel.setColumns(10);
		
		txtYearFab = new JTextField();
		txtYearFab.setBounds(168, 330, 69, 25);
		frame.getContentPane().add(txtYearFab);
		txtYearFab.setColumns(10);
		
		txtAirCond = new JTextField();
		txtAirCond.setBounds(168, 280, 69, 25);
		frame.getContentPane().add(txtAirCond);
		txtAirCond.setColumns(10);
		
		txtColour = new JTextField();
		txtColour.setBounds(168, 230, 69, 25);
		frame.getContentPane().add(txtColour);
		txtColour.setColumns(10);
		
		txtDoorsNr = new JTextField();
		txtDoorsNr.setBounds(168, 180, 69, 25);
		frame.getContentPane().add(txtDoorsNr);
		txtDoorsNr.setColumns(10);
		
		txtSeatsNr = new JTextField();
		txtSeatsNr.setBounds(168, 130, 69, 25);
		frame.getContentPane().add(txtSeatsNr);
		txtSeatsNr.setColumns(10);
		
		txtCarName = new JTextField();
		txtCarName.setBounds(168, 84, 200, 25);
		frame.getContentPane().add(txtCarName);
		txtCarName.setColumns(10);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				carReservation.getFrame().setVisible(true);
				txtCarName.setText(null);
				txtSeatsNr.setText(null);
				txtDoorsNr.setText(null);
				txtColour.setText(null);
				txtAirCond.setText(null);
				txtYearFab.setText(null);
				txtFuel.setText(null);
				txtPricePerDay.setText(null);
				txtOnSale.setText(null);
				
			}
		});
		btnBack.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		btnBack.setBounds(31, 20, 107, 35);
		frame.getContentPane().add(btnBack);
		
		labelShowImage = new JLabel("");
		labelShowImage.setBounds(275, 130, 501, 320);
		frame.getContentPane().add(labelShowImage);
		
		JLabel lblOnSale = new JLabel("On sale ? :");
		lblOnSale.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblOnSale.setBounds(10, 480, 119, 20);
		frame.getContentPane().add(lblOnSale);
		
		txtOnSale = new JTextField();
		txtOnSale.setColumns(10);
		txtOnSale.setBounds(168, 480, 69, 25);
		frame.getContentPane().add(txtOnSale);
	}

	public JFrame getFrame() {
		return frame;
	}
	
	public void getCarDetails(JComboBox<String> ComboBoxCar) {
		try {
			
			String query = "select * from cars where Car_Name=?";
			PreparedStatement pst = connection.prepareStatement(query);
			pst.setString(1, (String)ComboBoxCar.getSelectedItem());
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()) {
				txtCarName.setText(rs.getString("Car_Name"));
				txtSeatsNr.setText(rs.getString("Seats_Nr"));
				txtDoorsNr.setText(rs.getString("Doors_Nr"));
				txtColour.setText(rs.getString("Colour"));
				txtAirCond.setText(rs.getString("AirCond"));
				txtYearFab.setText(rs.getString("Year_Fab"));
				txtFuel.setText(rs.getString("Fuel"));
				txtPricePerDay.setText(rs.getString("PricePerDay"));
				txtOnSale.setText(rs.getString("OnSale"));
				byte[] img = rs.getBytes("Image");
				ImageIcon image = new ImageIcon(img);
				Image im = image.getImage();
				Image myImg = im.getScaledInstance(labelShowImage.getWidth(),labelShowImage.getHeight(), Image.SCALE_SMOOTH);
				ImageIcon newImage = new ImageIcon(myImg);
				labelShowImage.setIcon(newImage);
			}
			
			pst.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
