package application.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTable;

import database.connection.CarsDBConnection;
import net.proteanit.sql.DbUtils;

public class RefreshTable {
	private static Connection connection = CarsDBConnection.CarsdbConnector();

	public static void refreshTable(JTable table) {

		try {
			String query = "select CarID,Car_Name,Seats_Nr,Doors_Nr,Colour,Aircond,Year_Fab,Fuel,PricePerDay,OnSale,Image from cars";
			PreparedStatement pst = connection.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			pst.close();
			rs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
