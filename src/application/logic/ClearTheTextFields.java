package application.logic;

import javax.swing.JTextField;

public class ClearTheTextFields {

	public static void clearTextFields(JTextField txt, JTextField txt1, JTextField txt2, JTextField txt3,
			JTextField txt4, JTextField txt5, JTextField txt6, JTextField txt7, JTextField txt8) {
		txt.setText(null);
		txt1.setText(null);
		txt2.setText(null);
		txt3.setText(null);
		txt4.setText(null);
		txt5.setText(null);
		txt6.setText(null);
		txt7.setText(null);
		txt8.setText(null);
	}
}
