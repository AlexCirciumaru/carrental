package application.logic;

import javax.swing.JOptionPane;

public class CheckRentDetails {
	int pickUpDay, pickUpMonth, pickUpYear, dropOffDay, dropOffMonth, dropOffYear;
	String txtLocation;
	
	public CheckRentDetails() {
	}
	
	public boolean checkDetails(int pickUpDay, int pickUpMonth, int pickUpYear,int dropOffDay,int dropOffMonth,int dropOffYear,String txtLocation) {
		boolean validDetails = true;
		if(((pickUpDay > dropOffDay) && (pickUpMonth == dropOffMonth) && (pickUpYear <= dropOffYear)) || 
				((pickUpDay <= dropOffDay) && (pickUpMonth > dropOffMonth) && (pickUpYear <= dropOffYear)) ||
				((pickUpDay <= dropOffDay) && (pickUpMonth <= dropOffMonth) && (pickUpYear > dropOffYear))) {
			JOptionPane.showMessageDialog(null, "Invalid rent details.Please try again.");
			validDetails = false;
		}
		if(txtLocation.equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter a pick-up location.");
			validDetails = false;
		}
		return validDetails;
	}
}
