package app.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import application.logic.AddImageIntoDatabase;
import application.logic.CheckCarDetails;
import application.logic.CheckCarID;
import application.logic.ClearTheTextFields;
import application.logic.FillCarsJTable;
import application.logic.RefreshTable;
import application.logic.UpdatingPicture;
import database.connection.CarsDBConnection;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class CarsManagement {

	private JFrame frame;
	private JTextField txtPricePerDay, txtFuel, txtYearFab, txtAirCond, txtColour, txtDoorsNr, txtSeatsNr, txtOnSale;
	Connection connection = null;
	public static JTextField txtCarID,txtCarName;
	private JTable table;
	JFileChooser chooser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CarsManagement window = new CarsManagement();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CarsManagement() {
		connection = CarsDBConnection.CarsdbConnector();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1092, 548);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		/**
		 * Adauga o noua masina in baza de date.
		 * Verifica daca sunt completate toate campurile,altfel returneaza un mesaj.
		 * Permite utilizatorului sa selecteze poza dorita corespunzatoare masinii.
		 */
		JButton btnAddACar = new JButton("Add Car");
		btnAddACar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				PreparedStatement pst = null;
				try {
					String query = "insert into cars (Car_Name,Seats_Nr,Doors_Nr,Colour,Aircond,Year_Fab,Fuel,PricePerDay,OnSale,Image) values(?,?,?,?,?,?,?,?,?,?) ";
					pst = connection.prepareStatement(query);

					if (!(CheckCarDetails.checkTextFields(txtCarName.getText(), txtSeatsNr.getText(),
							txtDoorsNr.getText(), txtColour.getText(), txtAirCond.getText(), txtYearFab.getText(),
							txtFuel.getText(), txtPricePerDay.getText(), txtOnSale.getText()))) {
						JOptionPane.showMessageDialog(null, "Please complete all the fields.");
					} else {
						pst.setString(1, txtCarName.getText());
						pst.setString(2, txtSeatsNr.getText());
						pst.setString(3, txtDoorsNr.getText());
						pst.setString(4, txtColour.getText());
						pst.setString(5, txtAirCond.getText());
						pst.setString(6, txtYearFab.getText());
						pst.setString(7, txtFuel.getText());
						pst.setString(8, txtPricePerDay.getText());
						pst.setString(9, txtOnSale.getText());

						chooser = new JFileChooser("C:\\Users\\Alex\\Desktop\\Car_Pictures");
						FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
						chooser.setFileFilter(filter);
						int returnVal = chooser.showOpenDialog(null);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							pst.setBytes(10,
									AddImageIntoDatabase.readFile(chooser.getSelectedFile().getAbsolutePath()));
						}
						pst.execute();

						JOptionPane.showMessageDialog(null, "You successfully added a car.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						pst.close();
					} catch (Exception e2) {
						// stay silent
					}
				}

				ClearTheTextFields.clearTextFields(txtCarName, txtSeatsNr, txtDoorsNr, txtColour, txtAirCond,
						txtYearFab, txtPricePerDay, txtFuel, txtOnSale);
				RefreshTable.refreshTable(table);
			}
		});
		btnAddACar.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnAddACar.setBounds(394, 43, 149, 31);
		frame.getContentPane().add(btnAddACar);

		JButton btnDeleteACar = new JButton("Delete Car");
		btnDeleteACar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PreparedStatement pst = null;

				try {
					String query = "delete from cars where CarID='" + txtCarID.getText() + "' ";
					pst = connection.prepareStatement(query);

					if (txtCarID.getText().equals(null) || txtCarID.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Please enter a CarId in order to delete a car.");
					} else if (!(CheckCarID.checkCarID(txtCarID.getText()))) {
						JOptionPane.showMessageDialog(null, "Please enter a CarId that exists.");
						txtCarID.setText(null);
					} else {
						pst.execute();

						JOptionPane.showMessageDialog(null, "Car deleted from database.");
						txtCarID.setText(null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						pst.close();
					} catch (Exception e2) {
						// stay silent
					}
				}
				RefreshTable.refreshTable(table);
			}
		});
		btnDeleteACar.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnDeleteACar.setBounds(804, 43, 178, 31);
		frame.getContentPane().add(btnDeleteACar);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				frame.dispose();
				GraphicInterface graphicInterface = new GraphicInterface();
				graphicInterface.getFrame().setVisible(true);
			}
		});
		btnBack.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnBack.setBounds(42, 21, 104, 31);
		frame.getContentPane().add(btnBack);

		JLabel lblCarID = new JLabel("Car ID :");
		lblCarID.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblCarID.setBounds(656, 84, 69, 29);
		frame.getContentPane().add(lblCarID);

		JLabel lblCarName = new JLabel("Car Name :");
		lblCarName.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblCarName.setBounds(10, 80, 104, 29);
		frame.getContentPane().add(lblCarName);

		JLabel lblSeatsNr = new JLabel("Seats Nr. :");
		lblSeatsNr.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblSeatsNr.setBounds(10, 130, 91, 20);
		frame.getContentPane().add(lblSeatsNr);

		JLabel lblDoorsNr = new JLabel("Doors Nr. :");
		lblDoorsNr.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblDoorsNr.setBounds(10, 180, 104, 20);
		frame.getContentPane().add(lblDoorsNr);

		JLabel lblColour = new JLabel("Colour :");
		lblColour.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblColour.setBounds(10, 230, 79, 20);
		frame.getContentPane().add(lblColour);

		JLabel lblAirCond = new JLabel("Air Cond. :");
		lblAirCond.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblAirCond.setBounds(10, 280, 104, 20);
		frame.getContentPane().add(lblAirCond);

		JLabel lblYearFab = new JLabel("Year Fab. :");
		lblYearFab.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblYearFab.setBounds(10, 330, 104, 20);
		frame.getContentPane().add(lblYearFab);

		JLabel lblFuel = new JLabel("Fuel :");
		lblFuel.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblFuel.setBounds(10, 380, 56, 20);
		frame.getContentPane().add(lblFuel);

		JLabel lblPricePerDay = new JLabel("Price Per Day(\u20AC) :");
		lblPricePerDay.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblPricePerDay.setBounds(10, 430, 155, 20);
		frame.getContentPane().add(lblPricePerDay);

		txtPricePerDay = new JTextField();
		txtPricePerDay.setBounds(168, 430, 69, 25);
		frame.getContentPane().add(txtPricePerDay);
		txtPricePerDay.setColumns(10);

		txtFuel = new JTextField();
		txtFuel.setBounds(168, 380, 69, 25);
		frame.getContentPane().add(txtFuel);
		txtFuel.setColumns(10);

		txtYearFab = new JTextField();
		txtYearFab.setBounds(168, 330, 69, 25);
		frame.getContentPane().add(txtYearFab);
		txtYearFab.setColumns(10);

		txtAirCond = new JTextField();
		txtAirCond.setBounds(168, 280, 69, 25);
		frame.getContentPane().add(txtAirCond);
		txtAirCond.setColumns(10);

		txtColour = new JTextField();
		txtColour.setBounds(168, 230, 69, 25);
		frame.getContentPane().add(txtColour);
		txtColour.setColumns(10);

		txtDoorsNr = new JTextField();
		txtDoorsNr.setBounds(168, 180, 69, 25);
		frame.getContentPane().add(txtDoorsNr);
		txtDoorsNr.setColumns(10);

		txtSeatsNr = new JTextField();
		txtSeatsNr.setBounds(168, 130, 69, 25);
		frame.getContentPane().add(txtSeatsNr);
		txtSeatsNr.setColumns(10);

		txtCarName = new JTextField();
		txtCarName.setBounds(168, 84, 200, 25);
		frame.getContentPane().add(txtCarName);
		txtCarName.setColumns(10);

		txtCarID = new JTextField();
		txtCarID.setBounds(735, 88, 96, 25);
		frame.getContentPane().add(txtCarID);
		txtCarID.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(341, 135, 663, 286);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		// table.setEnabled(false);
		table.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table);
		FillCarsJTable.fillTheJTable(table);

		JButton btnUpdateCar = new JButton("Update Car");
		btnUpdateCar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				PreparedStatement pst = null;
				try {
					String query = "Update cars set Car_Name='" + txtCarName.getText() + "' ,Seats_Nr='"
							+ txtSeatsNr.getText() + "' ,Doors_Nr='" + txtDoorsNr.getText() + "' ,Colour='"
							+ txtColour.getText() + "' ,AirCond='" + txtAirCond.getText() + "' ,Year_Fab='"
							+ txtYearFab.getText() + "' ,Fuel='" + txtFuel.getText() + "' ,PricePerDay= '"
							+ txtPricePerDay.getText() + "' ,OnSale='" + txtOnSale.getText() + "' where CarID='"
							+ txtCarID.getText() + "' ";
					pst = connection.prepareStatement(query);

					if (txtCarID.getText().equals(null) || txtCarID.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Please enter a CarId in order to delete a car.");
					} else if (!(CheckCarDetails.checkTextFields(txtCarName.getText(), txtSeatsNr.getText(),
							txtDoorsNr.getText(), txtColour.getText(), txtAirCond.getText(), txtYearFab.getText(),
							txtFuel.getText(), txtPricePerDay.getText(), txtOnSale.getText()))) {
						JOptionPane.showMessageDialog(null,
								"You need to complete all the fields in order to finish the Update.");
					} else if (!(CheckCarID.checkCarID(txtCarID.getText()))) {
						JOptionPane.showMessageDialog(null, "Please enter a CarId that exists.");
						txtCarID.setText(null);
					} else {
						chooser = new JFileChooser("C:\\Users\\Alex\\Desktop\\Car_Pictures");
						FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
						chooser.setFileFilter(filter);
						int returnVal = chooser.showOpenDialog(null);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							UpdatingPicture object = new UpdatingPicture();
							object.updatePicture(chooser.getSelectedFile().getAbsolutePath());
							pst.execute();
						}

						JOptionPane.showMessageDialog(null, "Data updated successfully.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						pst.close();
					} catch (Exception e2) {
						// stay silent
					}
				}

				ClearTheTextFields.clearTextFields(txtCarName, txtSeatsNr, txtDoorsNr, txtColour, txtAirCond,
						txtYearFab, txtPricePerDay, txtFuel, txtOnSale);
				RefreshTable.refreshTable(table);
			}
		});
		btnUpdateCar.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnUpdateCar.setBounds(602, 43, 149, 28);
		frame.getContentPane().add(btnUpdateCar);

		JLabel lblOnSale = new JLabel("On sale ? :");
		lblOnSale.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblOnSale.setBounds(10, 480, 119, 20);
		frame.getContentPane().add(lblOnSale);

		txtOnSale = new JTextField();
		txtOnSale.setColumns(10);
		txtOnSale.setBounds(168, 480, 69, 25);
		frame.getContentPane().add(txtOnSale);

		JLabel lblEnterAnInt = new JLabel("(Enter an int)");
		lblEnterAnInt.setBounds(247, 130, 84, 19);
		frame.getContentPane().add(lblEnterAnInt);

		JLabel label = new JLabel("(Enter an int)");
		label.setBounds(247, 186, 84, 19);
		frame.getContentPane().add(label);

		JLabel label_1 = new JLabel("(Enter true or false)");
		label_1.setBounds(247, 486, 121, 19);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("(Enter an int)");
		label_2.setBounds(247, 336, 84, 19);
		frame.getContentPane().add(label_2);
		
		JLabel lblupdateAndDelete = new JLabel("(Update and Delete a car by the CarID)");
		lblupdateAndDelete.setBounds(670, 10, 240, 23);
		frame.getContentPane().add(lblupdateAndDelete);
	}

	public JFrame getFrame() {
		return frame;
	}
}
