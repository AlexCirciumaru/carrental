package app.gui;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import application.logic.CheckTextFields;
import application.logic.CheckTheUsername;
import application.logic.HashUtil;
import database.connection.SQLiteConnection;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.awt.event.ActionEvent;

public class CreateAccount {

	private JFrame frame;
	private JTextField txtLastName,txtFirstName,txtEmail,txtPhoneNumber,txtUserName,txtPassword;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateAccount window = new CreateAccount();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CreateAccount() {
		initialize();
		connection = SQLiteConnection.dbConnector();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 498);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblCustomersInformation = new JLabel("Customer's Information :");
		lblCustomersInformation.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 25));
		lblCustomersInformation.setBounds(48, 52, 294, 31);
		frame.getContentPane().add(lblCustomersInformation);

		JLabel lblLastName = new JLabel("Last Name :");
		lblLastName.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblLastName.setBounds(48, 140, 104, 25);
		frame.getContentPane().add(lblLastName);

		JLabel lblFirstName = new JLabel("First Name :");
		lblFirstName.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblFirstName.setBounds(48, 190, 104, 25);
		frame.getContentPane().add(lblFirstName);

		JLabel lblEmail = new JLabel("E-mail :");
		lblEmail.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblEmail.setBounds(48, 240, 90, 25);
		frame.getContentPane().add(lblEmail);

		JLabel lblPhoneNumber = new JLabel("Phone Number :");
		lblPhoneNumber.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblPhoneNumber.setBounds(48, 290, 136, 25);
		frame.getContentPane().add(lblPhoneNumber);

		txtLastName = new JTextField();
		txtLastName.setBounds(212, 140, 130, 22);
		frame.getContentPane().add(txtLastName);
		txtLastName.setColumns(10);

		txtFirstName = new JTextField();
		txtFirstName.setBounds(212, 190, 130, 22);
		frame.getContentPane().add(txtFirstName);
		txtFirstName.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setBounds(212, 240, 130, 22);
		frame.getContentPane().add(txtEmail);
		txtEmail.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBounds(212, 290, 130, 22);
		frame.getContentPane().add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		JLabel lblAccountDetails = new JLabel("Account Details :");
		lblAccountDetails.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 25));
		lblAccountDetails.setBounds(529, 54, 201, 26);
		frame.getContentPane().add(lblAccountDetails);

		JLabel lblUsername = new JLabel("Username :");
		lblUsername.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblUsername.setBounds(428, 169, 104, 19);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Sitka Small", Font.BOLD | Font.ITALIC, 15));
		lblPassword.setBounds(428, 219, 104, 25);
		frame.getContentPane().add(lblPassword);

		txtUserName = new JTextField();
		txtUserName.setBounds(577, 166, 142, 22);
		frame.getContentPane().add(txtUserName);
		txtUserName.setColumns(10);

		txtPassword = new JTextField();
		txtPassword.setBounds(577, 219, 142, 22);
		frame.getContentPane().add(txtPassword);
		txtPassword.setColumns(10);

		JButton btnCreateYourAccount = new JButton("Create Your Account");
		btnCreateYourAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				PreparedStatement pst = null;
				try {
					String query = "insert into accounts (Last_Name,First_Name,Email,Phone_Number,Username,Password) values (?,?,?,?,?,?)";
					pst = connection.prepareStatement(query);
					if (!(CheckTextFields.checkFields(txtLastName.getText(), txtFirstName.getText(), txtEmail.getText(),
							txtPhoneNumber.getText(), txtUserName.getText(), txtPassword.getText()))) {
						JOptionPane.showMessageDialog(null, "You need to complete all the fields.");
					} else if (CheckTheUsername.checkUsername(txtUserName.getText())) {
						JOptionPane.showMessageDialog(null, "This username already exists.Please choose another.");
						txtUserName.setText(null);
						txtPassword.setText(null);
					}else {
						pst.setString(1, txtLastName.getText());
						pst.setString(2, txtFirstName.getText());
						pst.setString(3, txtEmail.getText());
						pst.setString(4, txtPhoneNumber.getText());
						pst.setString(5, txtUserName.getText());
						pst.setString(6, HashUtil.sha256(txtPassword.getText()));
						pst.execute();

						JOptionPane.showMessageDialog(null,
								"You successfully created your account.Now you can log in.");
						
						frame.dispose();
						Login loginSystem = new Login();
						loginSystem.getFrame().setVisible(true);
					} 
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
					try {
						pst.close();
					} catch (Exception e2) {
						// stay silent
					}
				}
			}
		});
		btnCreateYourAccount.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 20));
		btnCreateYourAccount.setBounds(467, 363, 229, 44);
		frame.getContentPane().add(btnCreateYourAccount);
	}

	public JFrame getFrame() {
		return frame;
	}

}
