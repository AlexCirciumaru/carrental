package app.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import application.logic.HashUtil;
import database.connection.SQLiteConnection;

import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField txtUsername;
	private JPasswordField passwordField;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		connection = SQLiteConnection.dbConnector();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 628, 414);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);

		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		lblLogin.setBounds(288, 31, 106, 35);
		getFrame().getContentPane().add(lblLogin);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblUsername.setBounds(109, 134, 100, 22);
		getFrame().getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblPassword.setBounds(109, 189, 100, 22);
		getFrame().getContentPane().add(lblPassword);

		txtUsername = new JTextField();
		txtUsername.setBounds(262, 134, 162, 24);
		getFrame().getContentPane().add(txtUsername);
		txtUsername.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(262, 189, 162, 22);
		getFrame().getContentPane().add(passwordField);

		JSeparator separatorDown = new JSeparator();
		separatorDown.setBounds(44, 258, 515, 2);
		getFrame().getContentPane().add(separatorDown);

		JSeparator separatorUp = new JSeparator();
		separatorUp.setBounds(44, 96, 515, 2);
		getFrame().getContentPane().add(separatorUp);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query = "select * from accounts where Username=? and Password=? ";
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, txtUsername.getText());
					String pass = new String(passwordField.getPassword());
					pass = HashUtil.sha256(pass);
					pst.setString(2, pass);
					ResultSet rs = pst.executeQuery();
					if (rs.next()) {
						JOptionPane.showMessageDialog(null, "You successfully logged in.");
						RentTheCar.getUsername = txtUsername.getText();
						frame.dispose();
						GraphicInterface graphicInterface = new GraphicInterface();
						graphicInterface.getFrame().setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "Invalid login details.Please try again.");
						passwordField.setText(null);
						txtUsername.setText(null);
					}
					rs.close();
					pst.close();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
					passwordField.setText(null);
					txtUsername.setText(null);
				}
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnLogin.setBounds(90, 296, 100, 35);
		getFrame().getContentPane().add(btnLogin);

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				txtUsername.setText(null);
				passwordField.setText(null);

			}
		});
		btnReset.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnReset.setBounds(260, 296, 106, 35);
		getFrame().getContentPane().add(btnReset);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				JFrame frmLogin = new JFrame("Exit");
				if (JOptionPane.showConfirmDialog(frmLogin, "Confirm if you want to exit", "LoginSystem ",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnExit.setBounds(440, 296, 103, 35);
		getFrame().getContentPane().add(btnExit);

		JButton btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.dispose();
				CreateAccount createAccount = new CreateAccount();
				createAccount.getFrame().setVisible(true);
			}
		});
		btnCreateAccount.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnCreateAccount.setBounds(23, 31, 186, 35);
		frame.getContentPane().add(btnCreateAccount);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
