package app.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;

import database.connection.CustomersDataBase;
import database.connection.SQLiteConnection;

import java.awt.Font;
import javax.swing.JCheckBox;

public class RentTheCar {

	private JFrame frame;
	Connection connection, accConnection = null;
	JTextArea txtPrint;
	private JButton btnCompleteYourRental;
	public static String getUsername, getSelectedCar, getPickUpLocation;
	public static int getPickUpMonth, getDropOffMonth, getPickUpDay, getDropOffDay, getPickUpYear, getDropOffYear;
	JCheckBox checkBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RentTheCar window = new RentTheCar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RentTheCar() {
		connection = SQLiteConnection.dbConnector();
		initialize();
		accConnection = CustomersDataBase.dbConnector();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 888, 508);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 20));
		btnBack.setBounds(24, 389, 145, 40);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.dispose();
				CarReservation carReservation = new CarReservation();
				carReservation.getFrame().setVisible(true);
			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(btnBack);

		JButton btnPrintDetails = new JButton("Print Details");
		btnPrintDetails.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 20));
		btnPrintDetails.setBounds(341, 10, 160, 41);
		btnPrintDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String email, lastName, firstName, phoneNumber, pickUpDate, dropOffDate;
				email = lastName = firstName = phoneNumber = pickUpDate = dropOffDate = "";
				pickUpDate = getPickUpDay + "." + getPickUpMonth + "." + getPickUpYear;
				dropOffDate = getDropOffDay + "." + getDropOffMonth + "." + getDropOffYear;
				try {
					String query = "select * from accounts where Username=?";
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, getUsername);
					ResultSet rs = pst.executeQuery();
					email = rs.getString("Email");
					lastName = rs.getString("Last_Name");
					firstName = rs.getString("First_Name");
					phoneNumber = rs.getString("Phone_Number");
					txtPrint.append("\t\n Your rent details are : \n\n" + "Last Name : " + lastName + "\nFirst Name : "
							+ firstName + "\nEmail : " + email + "\nPhone Number : " + phoneNumber
							+ "\nPick-up Location : " + getPickUpLocation + "\nSelected Car :" + getSelectedCar
							+ "\nPick-up Date : " + pickUpDate + "\nDrop-off Date : " + dropOffDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnPrintDetails);

		txtPrint = new JTextArea();
		txtPrint.setBounds(208, 61, 413, 368);
		frame.getContentPane().add(txtPrint);

		btnCompleteYourRental = new JButton("Complete your rental ");
		btnCompleteYourRental.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (!checkBox.isSelected()) {
					JOptionPane.showMessageDialog(null, "Please go to the customers section and check if your car is available.");
				} else {
					String lastName, firstName, pickUpDate, dropOffDate;
					lastName = firstName = pickUpDate = dropOffDate = "";
					try {
						String anotherQuery = "select * from accounts where Username=?";
						PreparedStatement anotherPst = connection.prepareStatement(anotherQuery);
						anotherPst.setString(1, getUsername);
						ResultSet rs = anotherPst.executeQuery();
						lastName = rs.getString("Last_Name");
						firstName = rs.getString("First_Name");

						String query = "insert into customers (LastName,FirstName,PickUpDate,DropOffDate,RentedCar) values (?,?,?,?,?)";
						PreparedStatement pst = accConnection.prepareStatement(query);
						pickUpDate = getPickUpDay + "." + getPickUpMonth + "." + getPickUpYear;
						dropOffDate = getDropOffDay + "." + getDropOffMonth + "." + getDropOffYear;

						pst.setString(1, lastName);
						pst.setString(2, firstName);
						pst.setString(3, pickUpDate);
						pst.setString(4, dropOffDate);
						pst.setString(5, getSelectedCar);

						pst.execute();

						JOptionPane.showMessageDialog(null, "You successfully rented your car.");
						frame.dispose();
						GraphicInterface graphicInterface = new GraphicInterface();
						graphicInterface.getFrame().setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		btnCompleteYourRental.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 20));
		btnCompleteYourRental.setBounds(631, 389, 231, 40);
		frame.getContentPane().add(btnCompleteYourRental);

		checkBox = new JCheckBox("I checked.");
		checkBox.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		checkBox.setBounds(693, 141, 115, 35);
		frame.getContentPane().add(checkBox);

		JTextArea txtCheckArea = new JTextArea();
		txtCheckArea.setBounds(650, 71, 190, 53);
		frame.getContentPane().add(txtCheckArea);
		txtCheckArea.append("Please check if your car is available \nfor your desired period!!!");
	}

	public JFrame getFrame() {
		return frame;
	}
}
