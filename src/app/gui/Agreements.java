package app.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Agreements {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Agreements window = new Agreements();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * Create the agreements section
	 */
	public Agreements() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 856, 499);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				GraphicInterface graphicInterface = new GraphicInterface();
				graphicInterface.getFrame().setVisible(true);
			}
		});
		btnBack.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnBack.setBounds(33, 385, 121, 46);
		frame.getContentPane().add(btnBack);

		JTextPane txtAgreements = new JTextPane();
		txtAgreements.setText(
				"Termeni de \u00EEnchiriere ma\u015Fini\r\nAutomotive ofer\u0103 servicii de \u00EEnchiriere ma\u0219ini, din flota proprie de peste 350 autovehicule noi, din 16 m\u0103rci corespunz\u0103toare tuturor claselor de m\u0103rime, cu respectarea urm\u0103torilor termeni \u0219i condi\u021Bii:\r\n\r\nClientul care \u00EEnchiriaz\u0103 ma\u0219ina trebuie s\u0103 aib\u0103 minim 21 de ani \u00EEmplini\u021Bi la data \u00EEncheierii contractului de rent a car (se va prezenta CI/buletin sau pa\u0219aport) precum \u0219i permis de conducere cu vechime de cel pu\u021Bin 1 an. Autoturismele oferite de Automotive nu pot fi \u00EEnchiriate de soferi \u00EEncep\u0103tori.\r\nPermisul de conducere al persoanei care \u00EEnchiriaz\u0103 ma\u0219ina trebuie s\u0103 fie valabil pe teritoriul Rom\u00E2niei (permise UE)\r\nResponsabilitatea \u00EEnc\u0103lc\u0103rii legilor de circula\u021Bie revine \u00EEn exclusivitate conduc\u0103torului auto care a \u00EEnc\u0103lcat cu sau f\u0103r\u0103 bun\u0103 \u0219tiin\u021B\u0103 norma respectiv\u0103, acesta suport\u00E2nd cuantumul amenzii \u0219i punctele de amenda corespunzatoare.\r\nIe\u0219irea din \u021Bar\u0103 cu autoturismele \u00EEnchiriate de la Automotive Rent a Car se poate face numai dupa acceptul companiei noastre, \u00EEn scris.\r\nPentru confirmarea rezerv\u0103rii se poate solicita un avans de 50 euro, suma care se va sc\u0103dea din valoarea total\u0103 a zilelor de \u00EEnchiriat. \u00CEn cazul anularii rezerv\u0103rii sau neprezent\u0103rii la preluarea ma\u015Finii, suma achitat\u0103 ca avans nu se va restitui.\r\n\r\n\r\nPlata garan\u0163iei\r\nPlata garan\u021Biei se face \u00EEn lei (la cursul BNR din ziua pl\u0103\u021Bii), EUR sau USD.\r\n\r\nRestituirea garan\u021Biei se va face exclusiv \u00EEn moneda \u00EEn care a fost achitat\u0103. Garan\u021Bia va fi utilizat\u0103 pentru penalizare, \u00EEn cazuri de distrugeri inten\u021Bionate constatate, la predarea ma\u0219inii \u00EEnchiriate, \u00EEn interiorul sau exteriorul autoturismului, pierderi ale documentelor sau cheilor autoturismului, furt al p\u0103r\u021Bilor autoturismului etc.\r\n\r\nGaran\u021Bia achitat\u0103 la \u00EEncheierea contractului de \u00EEnchiriere auto va fi returnat\u0103 la recuperarea autoturismului \u00EEn bun\u0103 stare de func\u021Bionare \u0219i de caroserie, cu toate documentele, accesoriile \u0219i echipamentele cu care a fost predat clientului.\r\n\r\nAcesta este obligat s\u0103 restituie autoturismul la data \u0219i loca\u021Bia prevazut\u0103 \u00EEn contractul de \u00EEnchiriere, cu rezervorul plin.\r\n\r\nClientul trebuie s\u0103 foloseasc\u0103 pentru alimentarea autoturismului numai combustibil achizi\u021Bionat din sta\u021Biile de carburan\u021Bi autorizate.\r\n\r\n\r\nAccidente sau avarii\r\n\u00CEn caz de accident sau avarie din vina clientului (inclusiv situa\u021Bia \u00EEn care autoturismul este g\u0103sit lovit \u00EEn parcare) se va pl\u0103ti suma de 500 Euro + TVA sau 20% din valoarea daunei \u00EEn cazul \u00EEn care c/val repara\u021Biei este mai mare de 5000 euro, justificat\u0103 prin deviz de repara\u021Bie de la unitatea service a m\u0103rcii respective. \u00CEn caz de avarie care se dovede\u0219te (prin raportul de la poli\u021Bie) ca nu este din culpa clientului, se va plati suma de o sut\u0103 euro plus TVA pentru deschiderea dosarului de dauna precum \u0219i contravaloarea zilelor de sta\u021Bionare pentru remediere (contravaloare zile sta\u021Bionare = nr zile sta\u021Bionare X chirie/zi) dar nu mai mult dec\u00E2t valoarea garan\u021Biei. Sumele men\u021Bionate se pot achita cash sau se pot re\u021Bine cu cardul (prezent sau nu) care a fost folosit pentru achitarea ini\u021Biala a chiriei/garan\u021Biei.\r\n\r\nCompania noastra se obliga ca in cazul in care c/val remedierii avariilor provocate de client este mai mica decat valoarea garantiei retinute la returnarea masinii sa restituie diferenta, justificata prin deviz estimativ de la unitatea de service a marcii respective.qualification requirements.");
		txtAgreements.setBounds(159, 38, 535, 374);
		frame.getContentPane().add(txtAgreements);

		JScrollPane scrollPane = new JScrollPane(txtAgreements);
		scrollPane.setBounds(164, 22, 556, 391);

		frame.getContentPane().add(scrollPane);
	}

	public JFrame getFrame() {
		return frame;
	}
}
