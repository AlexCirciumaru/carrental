package app.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;

import application.logic.ShowDatabaseInJTable;
import database.connection.CustomersDataBase;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;

public class CustomersSection {

	private JFrame frame;
	Connection connection = null;
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomersSection window = new CustomersSection();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomersSection() {
		connection = CustomersDataBase.dbConnector();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 855, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				GraphicInterface graphicInterface = new GraphicInterface();
				graphicInterface.getFrame().setVisible(true);
			}
		});
		btnBack.setBounds(39, 358, 115, 35);
		frame.getContentPane().add(btnBack);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(164, 82, 608, 309);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setEnabled(false);
		ShowDatabaseInJTable.fillTheJTable(table);
		
		JLabel lblCustomers = new JLabel("Customers :");
		lblCustomers.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		lblCustomers.setBounds(423, 28, 129, 35);
		frame.getContentPane().add(lblCustomers);
	}

	public JFrame getFrame() {
		return frame;
	}
}
