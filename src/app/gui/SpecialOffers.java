package app.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;

import application.logic.CheckRentDetails;
import application.logic.FillSalesComboBox;
import application.logic.ShowCarsOnSaleDetails;
import database.connection.CarsDBConnection;

public class SpecialOffers {

	private JFrame frame;
	JMonthChooser pickUpMonth,dropOffMonth;
	JDayChooser pickUpDay,dropOffDay;
	JYearChooser pickUpYear,dropOffYear;
	JComboBox<String> comboBox;
	FillSalesComboBox fillTheComboBox = new FillSalesComboBox();
	CheckRentDetails checkRentDetails = new CheckRentDetails();
	private JLabel lblEnterThePickup; 
	private JTextField txtLocation;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SpecialOffers window = new SpecialOffers();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SpecialOffers() {
		connection = CarsDBConnection.CarsdbConnector();
		initialize();
		fillTheComboBox.fillComboBox(comboBox, "true");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 940, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblChooseACar = new JLabel("Choose a Car :");
		lblChooseACar.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		lblChooseACar.setBounds(20, 206, 116, 22);
		frame.getContentPane().add(lblChooseACar);
		
		JLabel lblPickupDate = new JLabel("Pick-up date :");
		lblPickupDate.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		lblPickupDate.setBounds(267, 208, 103, 30);
		frame.getContentPane().add(lblPickupDate);
		
		JLabel lblDropoffDate = new JLabel("Drop-off date :");
		lblDropoffDate.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		lblDropoffDate.setBounds(592, 208, 108, 30);
		frame.getContentPane().add(lblDropoffDate);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(123, 208, 127, 21);
		frame.getContentPane().add(comboBox);
		
		pickUpMonth = new JMonthChooser();
		pickUpMonth.getComboBox().setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		pickUpMonth.setBounds(380, 214, 133, 38);
		frame.getContentPane().add(pickUpMonth);
		
		pickUpDay = new JDayChooser();
		pickUpDay.setBounds(380, 252, 189, 133);
		frame.getContentPane().add(pickUpDay);
		
		dropOffMonth = new JMonthChooser();
		dropOffMonth.getComboBox().setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		dropOffMonth.setBounds(694, 214, 133, 38);
		frame.getContentPane().add(dropOffMonth);
		
		dropOffDay = new JDayChooser();
		dropOffDay.setBounds(694, 252, 189, 133);
		frame.getContentPane().add(dropOffDay);
		
		pickUpYear = new JYearChooser();
		pickUpYear.setBounds(515, 214, 50, 38);
		frame.getContentPane().add(pickUpYear);
		
		dropOffYear = new JYearChooser();
		dropOffYear.setBounds(827, 214, 57, 38);
		frame.getContentPane().add(dropOffYear);
		
		lblEnterThePickup = new JLabel("Enter the pick-up location :");
		lblEnterThePickup.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 15));
		lblEnterThePickup.setBounds(182, 408, 188, 19);
		frame.getContentPane().add(lblEnterThePickup);
		
		txtLocation = new JTextField();
		txtLocation.setBounds(390, 404, 157, 29);
		frame.getContentPane().add(txtLocation);
		txtLocation.setColumns(10);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				GraphicInterface graphicInterface = new GraphicInterface();
				graphicInterface.getFrame().setVisible(true);
			}
		});
		btnBack.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		btnBack.setBounds(43, 359, 103, 38);
		frame.getContentPane().add(btnBack);
		
		JButton btnContinue = new JButton("Continue");
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				RentTheCar.getSelectedCar = comboBox.getSelectedItem().toString();
				RentTheCar.getPickUpDay = pickUpDay.getDay();
				RentTheCar.getPickUpMonth = pickUpMonth.getMonth() + 1;
				RentTheCar.getDropOffDay = dropOffDay.getDay();
				RentTheCar.getDropOffMonth = dropOffMonth.getMonth() + 1;
				RentTheCar.getPickUpYear = pickUpYear.getYear();
				RentTheCar.getDropOffYear = dropOffYear.getYear();
				RentTheCar.getPickUpLocation = txtLocation.getText();
				if(checkRentDetails.checkDetails(pickUpDay.getDay(), (pickUpMonth.getMonth() + 1), pickUpYear.getYear(), dropOffDay.getDay(), (dropOffMonth.getMonth() + 1), dropOffYear.getYear(), txtLocation.getText())) {
					frame.dispose();
					RentTheCar carRent = new RentTheCar();
					carRent.getFrame().setVisible(true);
				}
			}
		});
		btnContinue.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		btnContinue.setBounds(730, 398, 153, 38);
		frame.getContentPane().add(btnContinue);
		
		JButton btnShowCarDetails = new JButton("Show car details");
		btnShowCarDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				ShowCarsOnSaleDetails showCarDetails = new ShowCarsOnSaleDetails();
				showCarDetails.getFrame().setVisible(true);
				showCarDetails.getCarDetails(comboBox);
			}
		});
		btnShowCarDetails.setFont(new Font("Book Antiqua", Font.BOLD | Font.ITALIC, 15));
		btnShowCarDetails.setBounds(20, 278, 165, 38);
		frame.getContentPane().add(btnShowCarDetails);
		
		JLabel lblImage = new JLabel("");
		lblImage.setBounds(20, 10, 410, 152);
		frame.getContentPane().add(lblImage);
		Image img = new ImageIcon(this.getClass().getResource("/20off.png")).getImage();
		lblImage.setIcon(new ImageIcon(img));
		getFrame().getContentPane().add(lblImage);
		
		JLabel lblSaleImage = new JLabel("");
		lblSaleImage.setBounds(420, 10, 410, 152);
		frame.getContentPane().add(lblSaleImage);
		Image imgSale = new ImageIcon(this.getClass().getResource("/sale-sign.jpg")).getImage();
		Image myImg = imgSale.getScaledInstance(lblSaleImage.getWidth(),lblSaleImage.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon newImage = new ImageIcon(myImg);
		lblSaleImage.setIcon(newImage);
	}

	public JFrame getFrame() {
		return frame;
	}

}
