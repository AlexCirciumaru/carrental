package app.gui;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Color;

public class GraphicInterface {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphicInterface window = new GraphicInterface();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GraphicInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().getContentPane().setBackground(new Color(119, 136, 153));
		getFrame().setBounds(100, 100, 789, 488);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		JButton btnCarReservation = new JButton("Car Reservation");
		btnCarReservation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				CarReservation carReservation = new CarReservation();
				carReservation.getFrame().setVisible(true);
			}
		});
		btnCarReservation.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnCarReservation.setBackground(new Color(105, 105, 105));
		btnCarReservation.setBounds(112, 202, 192, 136);
		getFrame().getContentPane().add(btnCarReservation);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				Login loginSystem = new Login();
				loginSystem.getFrame().setVisible(true);
			}
		});
		btnLogout.setForeground(new Color(178, 34, 34));
		btnLogout.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnLogout.setBackground(new Color(95, 158, 160));
		btnLogout.setBounds(112, 372, 273, 38);
		getFrame().getContentPane().add(btnLogout);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame frmLogin = new JFrame("Exit");
				if(JOptionPane.showConfirmDialog(frmLogin, "Confirm if you want to exit the application", "Car Rental ",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setForeground(new Color(255, 0, 0));
		btnExit.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnExit.setBackground(new Color(95, 158, 160));
		btnExit.setBounds(419, 372, 273, 38);
		getFrame().getContentPane().add(btnExit);
		
		JButton btnClients = new JButton("Agreements");
		btnClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				Agreements agreements = new Agreements();
				agreements.getFrame().setVisible(true);
			}
		});
		btnClients.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnClients.setBackground(new Color(105, 105, 105));
		btnClients.setBounds(500, 202, 192, 63);
		getFrame().getContentPane().add(btnClients);
		
		JButton btnNewButton = new JButton("Special offers");
		btnNewButton.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnNewButton.setBackground(new Color(105, 105, 105));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				SpecialOffers offersFrame = new SpecialOffers();
				offersFrame.getFrame().setVisible(true);
			}
		});
		btnNewButton.setBounds(315, 202, 175, 136);
		getFrame().getContentPane().add(btnNewButton);
		
		JLabel lblCarRental = new JLabel("Car Rental");
		lblCarRental.setForeground(new Color(0, 0, 139));
		lblCarRental.setBackground(SystemColor.activeCaptionBorder);
		lblCarRental.setFont(new Font("Century", Font.BOLD | Font.ITALIC, 45));
		lblCarRental.setBounds(370, 34, 281, 84);
		getFrame().getContentPane().add(lblCarRental);
		
		JLabel label = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/cars.jpg")).getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(21, 10, 281, 179);
		getFrame().getContentPane().add(label);
		
		JButton btnCustomers = new JButton("Customers");
		btnCustomers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				CustomersSection customersSection = new CustomersSection();
				customersSection.getFrame().setVisible(true);
			}
		});
		btnCustomers.setBackground(new Color(105, 105, 105));
		btnCustomers.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnCustomers.setBounds(500, 283, 192, 55);
		frame.getContentPane().add(btnCustomers);
		
		JButton btnCarsManagement = new JButton("Cars Management");
		btnCarsManagement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				CarsManagement carsManagement = new CarsManagement();
				carsManagement.getFrame().setVisible(true);
			}
		});
		btnCarsManagement.setFont(new Font("Arial Black", Font.BOLD | Font.ITALIC, 15));
		btnCarsManagement.setBackground(new Color(105, 105, 105));
		btnCarsManagement.setBounds(500, 128, 233, 45);
		frame.getContentPane().add(btnCarsManagement);
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}